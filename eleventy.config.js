module.exports = function(eleventyConfig) {
	// Copy the contents of the `public` folder to the output folder
	// For example, `./public/css/` ends up in `_site/css/`
	eleventyConfig.addPassthroughCopy({"./wwwroot/": "/"});
	eleventyConfig.addPassthroughCopy({"./admin/config.yml": "/admin/config.yml"});
}